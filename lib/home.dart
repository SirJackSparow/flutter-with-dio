import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app2/api/api_repository.dart';
import 'package:flutter_app2/api/models/post_models.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  ApiRepository apiRepository = ApiRepository();

  //dijalankan setelah class ini di create seperti init bloc pada kotlin
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Bimmislih"),backgroundColor: Colors.red,),
      body: Container(
      child: FutureBuilder(
        future: apiRepository.getDataPostFromApi,
         builder: (BuildContext context,AsyncSnapshot<List<post_models>> snapshot){
          switch(snapshot.connectionState){

            case ConnectionState.none:
              return Container(child: Center(child: CircularProgressIndicator(),),);
              break;
            case ConnectionState.waiting:
              return Container(child: Center(child: CircularProgressIndicator(),),);
              break;
            case ConnectionState.active:
              return Container(child: Center(child: CircularProgressIndicator(),),);
              break;
            case ConnectionState.done:
              if(snapshot.hasError){
                return Container(child: Container(child: Text("Something error"),),);
              }else {
                return BuildList(listData: snapshot.data,);
              }
              break;
          }

          return Container();
         },
      ),
    ),);
  }
}

class BuildList extends StatelessWidget{
   final List<post_models> listData;
    BuildList({Key key, this.listData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(child: ListView.builder(
        itemCount: listData.length,
        itemBuilder: (context,index){
          return ListTile(
            title: Text(listData[index].title),
            subtitle: Text(listData[index].body),
          );
        }
    ));
  }

}
