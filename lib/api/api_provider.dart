import 'dart:convert';

import 'package:dio/dio.dart' as http_dio;
import 'package:flutter_app2/api/models/post_models.dart';

class ApiProvider {
  http_dio.Response response;
  http_dio.Dio dio = http_dio.Dio();

  Future<List<post_models>> getDataPostFromApiAsync() async {
    response = await dio.get("http://jsonplaceholder.typicode.com/posts");

//    print(response.data);
//    print(response.statusMessage);
//    print(response.statusCode);

    final List rawData = jsonDecode(jsonEncode(response.data));

    if (response.statusCode == 200) {
      List<post_models> listPostModel =
          rawData.map((f) => post_models.fromJson(f)).toList();
      print("Test");
      print(listPostModel.length);
      return listPostModel;
    } else {
      return null;
    }
  }
}
