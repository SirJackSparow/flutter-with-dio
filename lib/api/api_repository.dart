import 'package:flutter_app2/api/api_provider.dart';
import 'package:flutter_app2/api/models/post_models.dart';

class ApiRepository {

  ApiProvider api = ApiProvider();

  Future<List<post_models>> get getDataPostFromApi => api.getDataPostFromApiAsync();

}
